from django.urls import path
from .views import (
    list_or_create_salespeople,
    delete_salespeople,
    list_or_create_customer,
    delete_customer,
    list_or_create_sale,
    delete_sale,
    list_automobileVOs,
)

urlpatterns = [
    path("autoVO/", list_automobileVOs, name="list_automobileVOs"),
    path("salespeople/", list_or_create_salespeople, name="list_or_create_salespeople"),
    path("salespeople/<int:pk>/", delete_salespeople, name="delete_salespeople"),
    path("customers/", list_or_create_customer, name="list_or_create_customer"),
    path("customers/<int:pk>/", delete_customer, name="delete_customer"),
    path("sales/", list_or_create_sale, name="list_or_create_sale"),
    path("sales/<int:pk>/", delete_sale, name="delete_sale")
]
