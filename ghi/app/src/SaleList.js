import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function SaleList() {
    const [sale, setSales] = useState([]);
    const navigate = useNavigate()


    const fetchSales = async () => {
        const salesApiUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(salesApiUrl);
        if (response.ok) {
            const data = await response.json();

            if (data === undefined) {
                return null;
            }

            setSales(data.sales)
        }
    }



    useEffect(() => {
        fetchSales();
    }, []);


    const handleOnClick = () => {
        navigate("/sales/create");
    }

    return (
        <>
        <div>
            <h1>Sale List</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sale.map(sale => (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
        <div><button onClick={handleOnClick} type="button" className='btn btn-primary' data-bs-toggle="button">record a new sale?</button></div>


        </>
    );
}

export default SaleList;
