import React, { useEffect, useState } from 'react';
function formatDate(dateString) {
    const options = {
        weekday: 'short',
        month: 'short',
        year: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        timeZoneName: 'short',
    };
    const formattedDate = new Date(dateString).toLocaleString(undefined, options);
    return formattedDate;
}
function VIPStatusCell({ vin }) {
    const [vipStatus, setVIPStatus] = useState('Loading...');

    useEffect(() => {
        const fetchVIPStatus = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/appointments/vipcheck/');
                if (response.ok) {
                    const data = await response.json();
                    const automobileVO = data.AutomobileVO.find((auto) => auto.vin === vin);
                    setVIPStatus(automobileVO && automobileVO.sold ? 'Yes' : 'No');
                } else {
                    console.error('Failed to fetch VIP information:', response.statusText);
                    setVIPStatus('No');
                }
            } catch (error) {
                console.error('Error fetching VIP information:', error.message);
                setVIPStatus('No');
            }
        };

        fetchVIPStatus();
    }, [vin]);

    return <td>{vipStatus}</td>;
}

function ScheduledAppointments() {
    const [appointments, setAppointments] = useState([]);

    const handleCancel = async (id) => {
        await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ status: 'canceled' }),
        });

        setAppointments((prevAppointments) =>
            prevAppointments.map((appointment) =>
                appointment.id === id ? { ...appointment, status: 'canceled' } : appointment
            )
        );
    };

    const handleFinish = async (id) => {
        await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ status: 'finished' }),
        });

        setAppointments((prevAppointments) =>
            prevAppointments.map((appointment) =>
                appointment.id === id ? { ...appointment, status: 'finished' } : appointment
            )
        );
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            }
        };

        fetchData();
    }, []);

    const scheduledAppointments = appointments.filter((appointment) => appointment.status === 'scheduled');

    return (
        <div>
            <h1>Scheduled Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vehicle Identification Number</th>
                        <th>VIP Status</th>
                        <th>Appointment Date/Time</th>
                        <th>Customer Name</th>
                        <th>Technician </th>
                        <th>Reason </th>
                        <th>Current Status </th>
                        <th>System ID </th>
                        <th colSpan="2">Action </th>
                    </tr>
                </thead>
                <tbody>
                    {scheduledAppointments.map((appointment) => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <VIPStatusCell vin={appointment.vin} appointment={appointment} />
                            <td>{formatDate(appointment.date_time)}</td>
                            <td>{appointment.customer}</td>
                            <td>{`${appointment.technician.last_name}, ${appointment.technician.first_name}`}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td>{appointment.id}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => handleCancel(appointment.id)}>
                                    Cancel
                                </button>
                            </td>
                            <td>
                                <button className="btn btn-success" onClick={() => handleFinish(appointment.id)}>
                                    Finish
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ScheduledAppointments;
