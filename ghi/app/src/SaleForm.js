import React, {useState, useEffect} from 'react';

const Form = {
    price: '',
    automobile: '',
    salesperson: '',
    customer: '',

};

function SaleForm () {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [autos, setAutos] = useState([]);
    const [formData, setFormData] = useState({ ...Form });


    const fetchAutoData = async () => {
        const AutoUrl = 'http://localhost:8090/api/autoVO/';
        const response = await fetch(AutoUrl);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.AutomobileVO);

        }
    }
    const fetchCustomerData = async () => {
        const CustomerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(CustomerUrl);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);

        }
    }
    const fetchSalespeopleData = async () => {
        const SalespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(SalespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);

        }
    }

    useEffect(() => {
        fetchAutoData();
        fetchCustomerData();
        fetchSalespeopleData();
    }, []);



    const handleInputChange = (e) => {
      const inputName = e.target.name;
      let value = e.target.value;


      if (inputName === 'price') {
          value = parseFloat(value);
      }


      else if (['automobile', 'salesperson', 'customer'].includes(inputName)) {
          value = parseInt(value, 10);
      }

      setFormData({
          ...formData,
          [inputName]: value
      });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();


    const url = `http://localhost:8090/api/sales/`;

    const fetchConfig = {
        method: 'post',
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch (url, fetchConfig);
    if (response.ok) {
        const selectedAuto = autos.find(auto => parseInt(auto.vin, 10) === formData.automobile);
        if (!selectedAuto) {
            return;
        }

        const autoUrl = `http://localhost:8100/api/automobiles/${selectedAuto.vin}/`;
        const autoFetchConfig = {
            method: 'put',
            body: JSON.stringify({ "sold": true }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const autoResponse = await fetch(autoUrl, autoFetchConfig);
        if (autoResponse.ok) {
        } else {
        }

        setFormData({ ...Form })
    }
};

    return (
    <>
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="car-form">
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
              <select onChange={handleInputChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an Automobile</option>
                {autos.filter(auto => auto.sold === false).map(auto => {
                    return (
                        <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                    )
                })}
            </select>
              </div>
              <div className="mb-3">
                <select onChange={handleInputChange} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a Salesperson</option>
                  {salespeople.map(salesperson => {
                    return (
                      <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleInputChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a Customer</option>
                  {customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );

                }
export default SaleForm;
