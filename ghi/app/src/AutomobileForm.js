import React, {useState, useEffect} from 'react';

const Form = {
    color: '',
    year: '',
    vin: '',
    model_id: '',

};

function AutomobileForm () {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({ ...Form });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    const handleInputChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;

        setFormData({
        ...formData,
        [inputName]: value
        });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();


        const url = `http://localhost:8100/api/automobiles/`;

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (url, fetchConfig);
        if (response.ok) {
            setFormData({ ...Form })
        }
    };

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Car</h1>
            <form onSubmit={handleSubmit} id="car-form">
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="name">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select onChange={handleInputChange} value={formData.model} required name="model_id" id="model_id" className="form-select">
                  <option value="model_id">Choose a Vehicle Model</option>
                  {models.map(model => {
                    return (
                      <option key={model.id} value={model.id}>{model.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

                }
export default AutomobileForm;
