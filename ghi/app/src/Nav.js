import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li> <NavLink className="nav-link" to="/new">Create Manufacturer</NavLink> </li>
            <li> <NavLink className="nav-link" to="/manufacturer">List Manufacturers</NavLink> </li>
            <li> <NavLink className="nav-link" to="/model">List Vehicle Models</NavLink> </li>
            <li> <NavLink className="nav-link" to="/create">Create Vehicle Model</NavLink> </li>
            <li> <NavLink className="nav-link" to="/techcreate">Add a Technician</NavLink> </li>
            <li> <NavLink className="nav-link" to="/techlist">Technician List</NavLink> </li>
            <li> <NavLink className="nav-link" to="/techdelete">Remove a Technician</NavLink> </li>
            <li> <NavLink className="nav-link" to="/appointmentcreate">Create an Appointment</NavLink> </li>
            <li> <NavLink className="nav-link" to="/scheduledappointments">Scheduled Appointments</NavLink> </li>
            <li> <NavLink className="nav-link" to="/servicehistory">Service History</NavLink> </li>
            <li><NavLink className="nav-link" to="/automobiles">List Automobiles</NavLink></li>
            <li><NavLink className="nav-link" to="/automobiles/create">Create Automobile</NavLink></li>
            <li><NavLink className="nav-link" to="/salespeople">List Salespeople</NavLink></li>
            <li><NavLink className="nav-link" to="/salespeople/create">Add Salesperson</NavLink></li>
            <li><NavLink className="nav-link" to="/customers">List Customers</NavLink></li>
            <li><NavLink className="nav-link" to="/customers/create">Add Customer</NavLink></li>
            <li><NavLink className="nav-link" to="/sales">List Sales</NavLink></li>
            <li><NavLink className="nav-link" to="/sales/create">Record Sale</NavLink></li>
            <li><NavLink className="nav-link" to="/salespeople/history">Salesperson History</NavLink></li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
