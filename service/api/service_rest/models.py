from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=200)


class Appointment(models.Model):
    STATUS_CHOICES = [
        ("finished", "Finished"),
        ("canceled", "Canceled"),
        ("scheduled", "Scheduled"),
    ]
    """
    Vin for appointment model will not be unique as a vin number
    can have mutiple appointments that will not be deleted.
    Technician is foreignKey that is protected, as appointments
    can be deleted but the technician will not be deleted.

    STATUS_CHOICES are limited to the above options for data consistancy,
    front end must utilize dropdowns to update the status
    """
    vin = models.CharField(max_length=17)
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    customer = models.CharField(max_length=200)
    status = status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default="scheduled"
    )

    technician = models.ForeignKey(
        Technician,
        related_name="tech",
        on_delete=models.PROTECT,
    )
