# CarCar
Is a automotive dealership management applicaiton that includes: inventory, sales, serivces and employees.
Team:

* Zack Hitchcock - Sales Microservice
* Ken Merritt - Service Microservice

## Table of Contents
1. [Project Setup & Insomnia Import](#setup)
2. [Design & Diagram](#design)
3. [UI - Frontend Design](#user-interface-design)
4. [Inventory Microservice & API](#inventory-microservice)
5. [Service Microservice & API](#service-microservice)
6. [Sales Microservice & API](#sales-microservice)

## Setup
Fork repository
Clone to local machine

```
git clone https://gitlab.com/hitchcockzack/project-beta.git
```
Create database volume then build and run containers in Docker:

```
cd project-beta
docker volume create beta-data
docker-compose build
code .
docker-compose up
```

Verify that all 7 containers are running and access the system at [http://localhost:3000/](http://localhost:3000/)


## Insomnia JSON Data
<ins>Note: Application must be on local machine prior to executing the following instructions:</ins>
1. Open the the Raw Insomnia import below
2. Copy the contents to you clipboard
3. Open Insomnia and create a new collection
4. Go to Insomnia home screen and click the elipsis and select import
5. Select clipboard.
6. Select "Scan" button
7. Select "Import" button
8. Interact with the API!

[Insomnia Import](Attachments/InsomniaExport_ProjectBeta.json)

[Back to Top](https://gitlab.com/hitchcockzack/project-beta#carcar)



## Design
This project was styled using bootstrap, with a modern and minimal design in mind. The focal points of the project are its features and architecture. We used a Navbar, and links from page to page as it made sense to do so, keeping the workflow of the projects intent in mind.


### CarCar has 3 microservices that interact with eachother: Sales, Inventory, Services.

![Alt text](Attachments/Carcarmodeldiagram.jpg)

## User Interface Design
### Images from the running application with simulated data





![Alt text](Attachments/CreateManufacturer.jpg)


![Alt text](<Attachments/Manufacturer List.jpg>)


![Alt text](Attachments/VehicleModleList.jpg)


![Alt text](Attachments/CreateVehicleModel.jpg)


![Alt text](Attachments/AddTechnician.jpg)


![Alt text](Attachments/Techlist.jpg)


![Alt text](Attachments/RemoveTech.jpg)


![Alt text](Attachments/Createappointment.jpg)


![Alt text](Attachments/ScheduledAppointments.jpg)


![Alt text](<Attachments/Service History.jpg>)


![Alt text](Attachments/AutomobileList.jpg)


![Alt text](Attachments/AddCar.jpg)


![Alt text](Attachments/Salespersonlist.jpg)


![Alt text](Attachments/AddSalesPerson.jpg)


![Alt text](Attachments/CustomerList.jpg)


![Alt text](Attachments/AddCustomer.jpg)


![Alt text](Attachments/SaleList.jpg)


![Alt text](Attachments/RecordNewSale.jpg)


![Alt text](Attachments/SalespersonHistroy.jpg)







[Back to Top](https://gitlab.com/hitchcockzack/project-beta#carcar)

## Inventory Microservice
The inventory microservice of CarCar is simple, but vital to the operation of our dealership. This is where we register and interact with the vehicle's manufacturer, model, and specific automobile details that we'll need to sell and service each one in the future. You can interact with the inventory API in the following ways:
### Manufacturer API:
| Action                     | Method | URL                                      |
| -------------------------- | ------ | ---------------------------------------- |
| List manufacturers         | GET    | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer      | POST   | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer| GET    | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

### Vehicle Model API:
| Action                        | Method | URL                                        |
| ----------------------------- | ------ | ------------------------------------------ |
| List vehicle models           | GET    | http://localhost:8100/api/models/          |
| Create a vehicle model        | POST   | http://localhost:8100/api/models/          |
| Get a specific vehicle model  | GET    | http://localhost:8100/api/models/:id/      |
| Update a specific vehicle model | PUT   | http://localhost:8100/api/models/:id/      |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/      |

### Automobile API:
| Action                    | Method | URL                                         |
| ------------------------- | ------ | ------------------------------------------- |
| List automobiles          | GET    | http://localhost:8100/api/automobiles/      |
| Create an automobile      | POST   | http://localhost:8100/api/automobiles/      |
| Get a specific automobile | GET    | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT  | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

## <ins>***Important note:***
the objects must be created in the following order to avoid errors-
1. Manufacturer
2. Vehicle Model
3. Automobile


[Back to Top](https://gitlab.com/hitchcockzack/project-beta#carcar)

## Service microservice

Service microservice allows for the creation of Technicians tracking thier name and employee ID number, allows those technicians to be assigned to customer appointments. <ins>***Appointments cannot be created without a technician assigned.***</ins>

Appointments created have 3 statuses: Scheduled, Finished and Canceled.  These statuses are used to filter the Scheduled Appointments list.  All appointments regardless of status are tracked in the Service History page.  This page can be filtered by VIN to gain insight to the Vehicles serivice history at your facility.  Additionally, the VIP status block is filtered through the AutomobileVO model to identify if a vehicle was sold by your Dealership.  This value object is kept up to date on a 60 second refresh via the Service Poller microservice.  This feature helps ensure that service after the sale is of the best quality, bringing customers back for future purchases.

***Important to note: There is not a user interface appointment delete function, while it is possible via Insomnia to delete an appointment if required, allowing for deletion of appointments would skew the Service History data.***

### Service Insomnia Description

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/serviceappointment/
| Service appointment detail | GET | http://localhost:8080/api/serviceappointment/<int:id>
| Service appointment history | GET | http://localhost:8080/api/servicehistory/<int:vin (OPTIONAL)>
| Create service appointment | POST | http://localhost:8080/api/serviceappointment/
| Delete service appointment | DELETE | http://localhost:8080/api/serviceappointment/<int:id>


#### *Get Service/AutomobileVO contents:*
* GET: http://localhost:8080/api/appointments/vipcheck/
* JSON Body: Returns entire contents of AutomobileVO
* JSON Response:
```JSON
{
	"AutomobileVO": [
		{
			"vin": "12345678912345678",
			"sold": true
		},
		{
			"vin": "1C3CC5FB2AN120174",
			"sold": false
		}
	]
}
```

#### *Create an appointment  :*
* POST:http://localhost:8080/api/appointments/
* JSON Body: Technician_id must be valid get from list technicians
```JSON
{
            "date_time": "2023-04-20T14:39:00.000Z",
            "reason": "broken glass. everywhere.",
            "vin": "2222",
            "customer": "Warren Longmire",
            "technician_id": 2
}
```
* JSON Response:
```JSON
{
	"vin": "2222",
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "broken glass. everywhere.",
	"customer": "Warren Longmire",
	"status": "scheduled",
	"technician": {
		"id": 2,
		"first_name": "Kennethh",
		"last_name": "Merrittt",
		"employee_id": "7417766"
	},
	"id": 17
}
```


#### *List Appointments  :*
* GET:http://localhost:8080/api/appointments/
* JSON Body:N/A List all appointments
* JSON Response:
```JSON
{
	"appointments": [
		{
			"vin": "6",
			"date_time": "2023-12-30T16:32:00+00:00",
			"reason": "its still on fire",
			"customer": "dummy mcdumerson",
			"status": "canceled",
			"technician": {
				"id": 2,
				"first_name": "Kennethh",
				"last_name": "Merrittt",
				"employee_id": "7417766"
			},
			"id": 12
		}
}
```

#### *Appointment Finish:*
* PUT: http://localhost:8080/api/appointments/10/finish/
* JSON Body: Appointment id is the bottom "id" field from list appointments. Id goes before finish in URL
```JSON
{
	"status": "finish"
}
```
* JSON Response:
```JSON
{
	"vin": "2",
	"date_time": "2024-03-18T16:26:00+00:00",
	"reason": "its still on fire",
	"customer": "checking form clear",
	"status": "finish",
	"technician": {
		"id": 2,
		"first_name": "Kennethh",
		"last_name": "Merrittt",
		"employee_id": "7417766"
	},
	"id": 10
}
```

#### *Appointment Scheduled :*
* PUT: http://localhost:8080/api/appointments/10/scheduled/
* JSON Body: Appointment id is the bottom "id" field from list appointments. Id goes before scheduled in URL
```JSON
{
	"status": "Scheduled"
}
```
* JSON Response:
```JSON
{
	"vin": "2",
	"date_time": "2024-03-18T16:26:00+00:00",
	"reason": "its still on fire",
	"customer": "checking form clear",
	"status": "Scheduled",
	"technician": {
		"id": 2,
		"first_name": "Kennethh",
		"last_name": "Merrittt",
		"employee_id": "7417766"
	},
	"id": 10
}
```

#### *Cancel Appointment :*
* PUT: http://localhost:8080/api/appointments/1/cancel/
* JSON Body: Appointment id is the bottom "id" field from list appointments. Id goes before cancel in URL
```JSON
{
	"status": "canceled"
}
```
* JSON Response:
```JSON
{
	"vin": "1C3CC5FB2AN120174",
	"date_time": "2024-01-25T17:30:33+00:00",
	"reason": "its on fire",
	"customer": "dummy mcdumerson",
	"status": "canceled",
	"technician": {
		"id": 4,
		"first_name": "Billy",
		"last_name": "Bob",
		"employee_id": "8675309"
	},
	"id": 1
}
```

#### *Delete Appointment :*
* DELETE:http://localhost:8080/api/appointments/3/
* JSON Body: N/A Appointment ID number at end of URL
* JSON Response:
```JSON
{
	" dummy mcdumerso": "appointment has been deleted"
}
```

#### *Create Technician :*
* POST: http://localhost:8080/api/technicians/
* JSON Body:
```JSON
{
            "first_name": "Uncle",
            "last_name": "Joe",
            "employee_id": 17760704
}
```
* JSON Response:
```JSON
{
	"first_name": "Uncle",
	"last_name": "Joe",
	"employee_id": 17760704,
	"id": 9
}
```


#### *List Technicians  :*
* GET: http://localhost:8080/api/technicians/
* JSON Body: N/A
* JSON Response:
```JSON
{
	"technicians": [
		{
			"first_name": "Uncle",
			"last_name": "Joe",
			"employee_id": "17760704",
			"id": 9
		}
	]
}
```

#### *Delete a Technician:*
* DELETE: http://localhost:8080/api/technicians/9/
* JSON Body: N/A final digit is "id"  from list technician
* JSON Response:
```JSON
{
	"message": "Technician removed sucessfully."
}
```
## Inventory microservice



### Service Insomnia Description
#### *Create Manufacturer :*
* POST: http://localhost:8100/api/manufacturers/
* JSON Body: Name cannot be the same as another manufacturer
```JSON
{
	"name": "Toyota"
}
```
* JSON Response:
```JSON
{
	"href": "/api/manufacturers/6/",
	"id": 6,
	"name": "Toyota"
}
```
#### *List Manufacturer  :*
* GET: http://localhost:8100/api/manufacturers/
* JSON Body: Lists all manufacturers
* JSON Response:
```JSON
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Chrysler"
		}
    ]
}
```

#### *Get Specific Manufacuturer  :*
* GET:http://localhost:8100/api/manufacturers/2/
* JSON Body: N/A id number at end of
* JSON Response: Will display error message if id does not exist
```JSON
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
```JSON
{
	"message": "Does not exist"
}
```

#### *Update Specific Manufacturer :*
* GET: http://localhost:8100/api/manufacturers/1/
* JSON Body: id at end of URL for manufacturer to be updated
```JSON
{
	"name": "Honda"
}
```
* JSON Response: Will display does not exist if id number is invalid
```JSON
{
	"name": "Honda"
}

```
```JSON
{
	"message": "Does not exist"
}
```

#### *Delete Manufacturer:*
* DELETE: http://localhost:8100/api/manufacturers/1/
* JSON Body: N/A ID number is at the end of the URL

* JSON Response:
```JSON
{
	"id": null,
	"name": "Honda"
}
```

#### *Create Vehicle Model:*
* POST: http://localhost:8100/api/models/
* JSON Body: Manufacturer_id must be valid
```JSON
{
  "name": "Accord",
  "picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
  "manufacturer_id": 3
}
```
* JSON Response:
```JSON
{
	"href": "/api/models/3/",
	"id": 3,
	"name": "Accord",
	"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Ford"
	}
}
```

#### *List Vehicle Model :*
* GET: http://localhost:8100/api/models/
* JSON Body:N/A
* JSON Response:
```JSON
{
	"models": [
    {
			"href": "/api/models/3/",
			"id": 3,
			"name": "Accord",
			"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/3/",
				"id": 3,
				"name": "Ford"
			}
		}
	]
}
```

#### *Get specific Vehicle model:*
* GET: 	http://localhost:8100/api/models/3/
* JSON Body: Id number at end of URL must be valid
* JSON Response:
```JSON
{
	"href": "/api/models/3/",
	"id": 3,
	"name": "Accord",
	"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Ford"
	}
}
```

#### *Update Specific Vehicle Model  :*
* PUT: http://localhost:8100/api/models/2/
* JSON Body: Id number of modle must be valid at end of URL
```JSON
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```
* JSON Response:
```JSON
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Ford"
	}
}
```

#### *Delete Vehicle Model :*
* DELETE: http://localhost:8100/api/models/2/
* JSON Body: N/A must be a valid Modle ID at end of URL
* JSON Response:
```JSON
{
	"id": null,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Ford"
	}
}
```

#### *Create Automobile  :*
* POST: http://localhost:8100/api/automobiles/
* JSON Body:
```JSON
{
	"color": "blue",
  "year": "2014",
  "vin": "123456789X1234567",
  "model_id": "3"
}
```
* JSON Response:
```JSON
{
	"href": "/api/automobiles/123456789X1234567/",
	"id": 3,
	"color": "blue",
	"year": "2014",
	"vin": "123456789X1234567",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "Accord",
		"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Ford"
		}
	},
	"sold": false
}
```

#### *List Automobiles :*
* GET: http://localhost:8100/api/automobiles/
* JSON Body:
* JSON Response:
```JSON
{
	"autos": [
		{
			"href": "/api/automobiles/123456789X1234567/",
			"id": 3,
			"color": "blue",
			"year": 2014,
			"vin": "123456789X1234567",
			"model": {
				"href": "/api/models/3/",
				"id": 3,
				"name": "Accord",
				"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/3/",
					"id": 3,
					"name": "Ford"
				}
			},
			"sold": false
		}
	]
}
```

#### *TITLE  :*
* GET:http://localhost:8100/api/automobiles/123456789X1234567/
* JSON Body: N/A automobile VIN must be at the end of the URL
* JSON Response:
```JSON
{
	"href": "/api/automobiles/123456789X1234567/",
	"id": 3,
	"color": "blue",
	"year": 2014,
	"vin": "123456789X1234567",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "Accord",
		"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Ford"
		}
	},
	"sold": false
}
```

#### *Update Automobiel  :*
* PUT: http://localhost:8100/api/automobiles/123456789X1234567/
* JSON Body: only fields to be updated are required, VIN number at end of URL required
```JSON
{
	"color": "blue",
  "sold": true
}
```
* JSON Response:
```JSON
{
	"href": "/api/automobiles/123456789X1234567/",
	"id": 3,
	"color": "blue",
	"year": 2014,
	"vin": "123456789X1234567",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "Accord",
		"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Ford"
		}
	},
	"sold": true
}
```

#### *TITLE  :*
* GET:http://localhost:8100/api/automobiles/123456789X1234567/
* JSON Body: NA Vin Required at end of URL
* JSON Response:
```JSON
	"href": "/api/automobiles/123456789X1234567/",
	"id": null,
	"color": "blue",
	"year": 2014,
	"vin": "123456789X1234567",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "Accord",
		"picture_url": "https://hips.hearstapps.com/hmg-prod/amv-prod-cad-assets/wp-content/uploads/2017/03/1986_Accord_3rd_Generation.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Ford"
		}
	},
	"sold": true
}
```

[Back to Top](https://gitlab.com/hitchcockzack/project-beta#carcar)
## Sales Microservice

The sales microservice in this project is responsible for tracking, creating, and maintaining the sales side of the CarCar dealership/application. Within it, we are able to interact with CarCar's salespeople, customers, and track which automobiles are sold between them.


The models, listed below.
1. **Salesperson**
	- contains the sales person ***first name, last name*** (character fields) and ***employee ID***, a positive integer field.
1. **Customer**
	- contains the customers ***first name, last name, address, and phone number*** (all character fields)
1. **AutomobileVO**
	- this is a value object of the Automobile object from the inventory API. See the <ins>***Sales Poller***</ins> below for more details.
1. **Sale**
	- contains the ***price***, a positive integer field
	- contains 3 ForeignKey relationships for:
		- automobile, which references an instance of the AutomobileVO object
		- salesperson, which references an instance of the specific salesperson object in the sales record
		- customer, which refers to an instance of the specific customer object in the sales record

<br>

The view functions in the backend allow you to list, create, and delete all of these objects. Specific JSON payload and JsonResponse examples for the sales microservice are shown below.

### <ins>Sales Poller:
	The sales poller is a function in my microservice that polls the inventory API every 60 seconds for an updated or new Automobile object from the inventory API. It recieves a "vin" property, which is a unique key for each automobile, and a "sold": false/true property, which I used later to list unsold automobiles in inventory when recording a new sale.

### Salespeople API:
| Action | Method | URL |
|------------------|---|----------------------------------|
|1. Create salespeople  |	**POST** |	http://localhost:8090/api/salespeople/  |
|2. List salespeople  |	**GET** |	http://localhost:8090/api/salespeople/  |
|3. Delete salespeople  |	**DELETE** |	http://localhost:8090/api/salespeople/:id/  |

### Customer API:
| Action | Method | URL |
|------------------|---|----------------------------------|
|1. Create customer  |	POST |	http://localhost:8090/api/customers/  |
|2. List customer  |	GET |	http://localhost:8090/api/customers/  |
|3. Delete customer  |	DELETE |	http://localhost:8090/api/customers/:id/  |


### Sales API:
| Action | Method | URL |
|------------------|---|----------------------------------|
|1. Create sale  |	POST |	http://localhost:8090/api/sales/  |
|2. List sales  | 	GET |	http://localhost:8090/api/sales/  |
|3. Delete sale  | 	DELETE |	http://localhost:8090/api/sales/:id  |

<br>
<br>
<br>
<br>



### Create Salespeople - JsonResponse:
<div style="display: flex; justify-content: space-between;">
<div style="flex: 1; padding-right: 10px;">

#### Input

<pre>
<code class="language-javascript">
{
	"first_name": "Zack",
	"last_name": "Hitchcock",
	"employee_id": 1234
}




</code>
</pre>

</div>
<div style="flex: 1; padding-left: 10px;">

#### Return

<pre>
<code class="language-javascript">
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Zack",
			"last_name": "Hitchcock",
			"employee_id": 1234
		}]
}
</code>
</pre>

</div>
</div>



### List Salespeople - JsonResponse:
```JSON
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Zack",
			"last_name": "Hitchcock",
			"employee_id": 1234
		}]
}
```
### Delete Salespeople - JsonResponse
```JSON
{
	"salesperson Zack Hitchcock, employee #1234": "has been deleted"
}
```
___
<br>

### Create Customers - JsonResponse:
<div style="display: flex; justify-content: space-between;">
<div style="flex: 1; padding-right: 10px;">

#### Input

<pre>
<code class="language-javascript">
{
	"first_name": "Ken",
	"last_name": "Merrit",
	"address": "123 Customer Ave.",
	"phone_number": "123-456 7890"
}




</code>
</pre>

</div>
<div style="flex: 1; padding-left: 10px;">

#### Return

<pre>
<code class="language-javascript">
{
	"id": 15,
	"first_name": "Ken",
	"last_name": "Merrit",
	"address": "123 Customer Ave.",
	"phone_number": "123-456 7890"
}



</code>
</pre>

</div>
</div>

### List Customers - JsonResponse:
```JSON
{
	"customers": [
		{
			"id": 15,
			"first_name": "Ken",
			"last_name": "Merrit",
			"address": "123 Customer Ave.",
			"phone_number": "123-456 7890"
		}]
}
```
### Delete Customers - JsonResponse
```JSON
{
	"customer Ken Merrit": "has been deleted"
}
```


___

<br>


### Record New Sale - JsonResponse:
Before submitting a form, you will notice that only vehicles that have `{"sold": false}` properties will populate in the automobile dropdown on the form.

When you submit, A "PUT" request is simultaneously sent to the Inventory microservice's corresponding Automobile object with the content `{"sold": true}` upon submitting the form.


60 seconds later or so, the poller will also be updated so the sales microservice Automobile VO object for that vin will now show as sold. Once that has happened, that automobile will no longer show up in the list of automobile vins that can be sold. This was implemented in the frontend using ternary operators and the filter function. Because it was done in the frontend, the feature only exists in the frontend. it is possible to sell a sold vehicle multiple times when interacting with the API in insomnia.
<div style="display: flex; justify-content: space-between;">
<div style="flex: 1; padding-right: 10px;">

#### Input

<pre>
<code class="language-javascript">
	{
		"price": 15000,
		"automobile": 1,
		"salesperson": 1234,
		"customer": 1

	}















</code>
</pre>

</div>
<div style="flex: 1; padding-left: 10px;">

#### Return

<pre>
<code class="language-javascript">
{
	"id": 1,
	"price": 15000,
	"automobile": {
		"id": 1,
		"vin": "1",
		"sold": true
	},
	"salesperson": {
		"id": 1,
		"first_name": "Zack",
		"last_name": "Hitchcock",
		"employee_id": 1234
	},
	"customer": {
		"id": 15,
		"first_name": "Ken",
		"last_name": "Merrit",
		"address": "123 Customer Ave.",
		"phone_number": "123-456 7890"
	}
}
</code>
</pre>

</div>
</div>

### List Sales - JsonResponse:
```JSON
{
	"sales": [
		{
			"id": 1,
			"price": 15000,
			"automobile": {
				"id": 1,
				"vin": "1",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "Zack",
				"last_name": "Hitchcock",
				"employee_id": 1234
			},
			"customer": {
				"id": 1,
				"first_name": "Ken",
				"last_name": "Merrit",
				"address": "123 Customer Ave.",
				"phone_number": "123-456 7890"
			}
		}
	]
}
```
### Delete a Sale - JsonResponse
```JSON
{
	"sale record: vin# '1' for $15000.00": "deleted"
}
```
[Back to Top](https://gitlab.com/hitchcockzack/project-beta#carcar)
